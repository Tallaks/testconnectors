using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class EventManager
{
	private static readonly Dictionary<Type, List<IEventListener>> _subscribers =
		new Dictionary<Type, List<IEventListener>>();

	private static readonly Dictionary<Type, List<Type>> _subscribersCache =
		new Dictionary<Type, List<Type>>();

	public static void Subscribe(IEventListener subscriber)
	{
		var subscriberTypes = GetSubscribersTypes(subscriber.GetType());
		foreach (var t in subscriberTypes)
			if (_subscribers.ContainsKey(t))
			{
				_subscribers[t].Add(subscriber);
			}
			else
			{
				_subscribers[t] = new List<IEventListener>();
				_subscribers[t].Add(subscriber);
			}
	}

	private static List<Type> GetSubscribersTypes(Type type)
	{
		if (_subscribersCache.ContainsKey(type))
			return _subscribersCache[type];

		var subscriberTypes = type
			.GetInterfaces()
			.Where(it => typeof(IEventListener).IsAssignableFrom(it) && it != typeof(IEventListener)).ToList();

		_subscribersCache[type] = subscriberTypes;
		return subscriberTypes;
	}

	public static void RaiseEvent<T>(Action<T> action)
		where T : IEventListener
	{
		if (!_subscribers.ContainsKey(typeof(T))) _subscribers[typeof(T)] = new List<IEventListener>();

		var subscribers = _subscribers[typeof(T)];
		foreach (var subscriber in subscribers)
			try
			{
				action.Invoke((T)subscriber);
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
	}
}