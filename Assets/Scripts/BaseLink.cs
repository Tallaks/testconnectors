using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public abstract class BaseLink : MonoBehaviour
{
	[SerializeField] protected Material _lineMaterial;
	[SerializeField] protected float _lineThickness = 0.1f;

	protected Connector _firstConnector;
	protected Mesh _lineMesh;
	protected Vector3 FirstPosition => _firstConnector.transform.position;

	protected virtual void DrawMesh(Vector3 secondPosition)
	{
		_lineMesh = new Mesh();

		var vertices = new Vector3[4];
		var uv = new Vector2[4];
		var trangles = new int[12];

		vertices[0] = FirstPosition - new Vector3(0, _lineThickness, 0);
		vertices[1] = FirstPosition + new Vector3(0, _lineThickness, 0);
		vertices[2] = secondPosition - new Vector3(0, _lineThickness, 0);
		vertices[3] = secondPosition + new Vector3(0, _lineThickness, 0);

		uv[0] = new Vector2(0, 0);
		uv[1] = new Vector2(0, 1);
		uv[2] = new Vector2(1, 0);
		uv[3] = new Vector2(1, 1);

		trangles[0] = 0;
		trangles[1] = 1;
		trangles[2] = 3;
		trangles[3] = 0;
		trangles[4] = 3;
		trangles[5] = 2;

		trangles[6] = 0;
		trangles[7] = 3;
		trangles[8] = 1;
		trangles[9] = 0;
		trangles[10] = 2;
		trangles[11] = 3;

		_lineMesh.vertices = vertices;
		_lineMesh.uv = uv;
		_lineMesh.triangles = trangles;

		GetComponent<MeshFilter>().mesh = _lineMesh;
	}
}