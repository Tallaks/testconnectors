using UnityEngine;

public class Platform : MonoBehaviour
{
	private Connectable _connectable;

	private void Awake()
	{
		_connectable = GetComponentInParent<Connectable>();
	}

	private void OnMouseDrag()
	{
		var plane = new Plane(_connectable.transform.up, _connectable.transform.position);
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (plane.Raycast(ray, out var distanceToPlane)) _connectable.SetPosition(ray.GetPoint(distanceToPlane));
	}
}