using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class Link : BaseLink
{
	private Connector _secondConnector;
	private Vector3 SecondPosition => _secondConnector.transform.position;

	private void Update()
	{
		DrawMesh(SecondPosition);
	}

	public void Init(Connector first, Connector second)
	{
		_firstConnector = first;
		_secondConnector = second;
		DrawMesh(SecondPosition);
		GetComponent<MeshRenderer>().material = _lineMaterial;
	}
}