﻿using UnityEngine;

public class Main : MonoBehaviour
{
	[SerializeField] private int _connectableNumber;

	public float Radius = 10;
	public int ConnectableNumber => _connectableNumber;

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (!Physics.Raycast(ray, out hit)) EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
		}
	}
}