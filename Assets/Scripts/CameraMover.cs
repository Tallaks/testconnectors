using UnityEngine;

public class CameraMover : MonoBehaviour
{
	[SerializeField] private float _moveSpeed = 1;
	[SerializeField] private float _rotationSpeed = 1;

	private void Update()
	{
		var verticalDelta = Input.GetAxis("Vertical");
		var horizontalDelta = Input.GetAxis("Horizontal");

		var delta = transform.TransformVector(new Vector3(horizontalDelta, 0, verticalDelta));
		transform.position += delta * Time.deltaTime * _moveSpeed;

		if (Input.GetKey(KeyCode.Q))
		{
			var rotation = transform.rotation;
			rotation.eulerAngles += new Vector3(0, _rotationSpeed * Time.deltaTime, 0);
			transform.rotation = rotation;
		}

		if (Input.GetKey(KeyCode.E))
		{
			var rotation = transform.rotation;
			rotation.eulerAngles -= new Vector3(0, _rotationSpeed * Time.deltaTime, 0);
			transform.rotation = rotation;
		}
	}
}