using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class PreviewLink : BaseLink
{
	private void Update()
	{
		RaycastHit hit;
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			var hitConnector = hit.transform.GetComponent<Connector>();
			if (hitConnector != null)
			{
				DrawMesh(hit.transform.position);
				return;
			}

			var plane = new Plane(transform.up, FirstPosition);
			if (plane.Raycast(ray, out var distanceToPlane)) DrawMesh(ray.GetPoint(distanceToPlane));
		}
		else
		{
			var plane = new Plane(transform.up, FirstPosition);
			if (plane.Raycast(ray, out var distanceToPlane)) DrawMesh(ray.GetPoint(distanceToPlane));
		}
	}

	public void Init(Connector first)
	{
		_firstConnector = first;
		DrawMesh(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		GetComponent<MeshRenderer>().material = _lineMaterial;
	}
}