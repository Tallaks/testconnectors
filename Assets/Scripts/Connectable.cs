using UnityEngine;

public class Connectable : MonoBehaviour
{
	public void SetPosition(Vector3 position)
	{
		transform.position = position;
	}
}