using UnityEngine;

public class Connector : MonoBehaviour, ISelectOnClick, IDeselectOnClick, ISecondSelectOnClick, IPreSelectOnMouseOver,
	IPreSelectCanceled
{
	[SerializeField] private Material _selected;
	[SerializeField] private Material _avaliable;
	[SerializeField] private Material _default;

	private void Awake()
	{
		EventManager.Subscribe(this);
	}

	public void OnDeselect()
	{
		GetComponent<MeshRenderer>().material = _default;
	}

	public void OnPreAddCancelled(Connector preAddedConnector)
	{
		if (preAddedConnector == this)
		{
			GetComponent<MeshRenderer>().material = _avaliable;
		}
	}

	public void OnPreSelect(Connector from, Connector preSelectedConnector)
	{
		if (preSelectedConnector == this)
			GetComponent<MeshRenderer>().material = _selected;
		else if (from != this) GetComponent<MeshRenderer>().material = _avaliable;
	}

	public void OnSecondSelect()
	{
		Debug.Log("SecondSelect");
		EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
	}

	public void OnClickSelection(Connector selectedConnector)
	{
		if (selectedConnector == this)
		{
			GetComponent<MeshRenderer>().material = _selected;
		}
		else
		{
			GetComponent<MeshRenderer>().material = _avaliable;
		}
	}
}