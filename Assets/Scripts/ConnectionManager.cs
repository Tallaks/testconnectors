using System;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionManager : MonoBehaviour, IDeselectOnClick
{
	[SerializeField] private GameObject _linkPrefab;
	[SerializeField] private GameObject _previewLinkPrefab;
	private readonly List<Tuple<Connector, Connector>> _pairsOfConnectors = new List<Tuple<Connector, Connector>>();
	private Connector _preSlectedConnector;
	private PreviewLink _previewLink;
	private Connector _selectedConnector;

	private void Awake()
	{
		EventManager.Subscribe(this);
	}

	private void Update()
	{
		if (HandleButtonPressed()) return;
		if (CheckForSelectedConnector()) return;
		HandleButtonPressing();
		HandleButtonReleased();
	}

	public void OnDeselect()
	{
		_selectedConnector = null;
		_preSlectedConnector = null;
	}

	private bool HandleButtonPressed()
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				var hitConnector = hit.transform.GetComponent<Connector>();
				if (DeselectIfNotOtherConnector(hitConnector) == false) return false;
				if (TrySelectFirstConnector(hitConnector)) return true;
				SelectSecondConnector(hitConnector);
			}
			else
			{
				EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
			}

			return true;
		}

		return false;
	}

	private bool CheckForSelectedConnector()
	{
		return _selectedConnector == null;
	}

	private void HandleButtonPressing()
	{
		if (Input.GetMouseButton(0))
		{
			CreatePreviewLink();
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				var hitConnector = hit.transform.GetComponent<Connector>();
				if (_selectedConnector == hitConnector) return;
				if (CancelPreviewSelectIfNoConnectorHit(hitConnector)) return;
				ChangePreSelectedConnector(hitConnector);
			}
			else
			{
				CancelPreSelectIfNoObjectHit();
			}
		}
	}

	private bool DeselectIfNotOtherConnector(Connector hitConnector)
	{
		if (hitConnector == null || hitConnector == _selectedConnector)
		{
			EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
			return false;
		}

		return true;
	}

	private bool TrySelectFirstConnector(Connector hitConnector)
	{
		if (_selectedConnector == null)
		{
			EventManager.RaiseEvent<ISelectOnClick>(k => k.OnClickSelection(hitConnector));
			_selectedConnector = hitConnector;
			return true;
		}

		return false;
	}

	private void SelectSecondConnector(Connector hitConnector)
	{
		if (!_pairsOfConnectors.Contains(new Tuple<Connector, Connector>(_selectedConnector, hitConnector)) &&
		    !_pairsOfConnectors.Contains(new Tuple<Connector, Connector>(hitConnector, _selectedConnector)))
		{
			var link = Instantiate(_linkPrefab, transform.localPosition, Quaternion.identity, transform);
			link.GetComponent<Link>().Init(_selectedConnector, hitConnector);
			_pairsOfConnectors.Add(new Tuple<Connector, Connector>(_selectedConnector, hitConnector));
		}

		EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
	}

	private void CancelPreSelectIfNoObjectHit()
	{
		EventManager.RaiseEvent<IPreSelectCanceled>(k => k.OnPreAddCancelled(_preSlectedConnector));
		_preSlectedConnector = null;
	}

	private void ChangePreSelectedConnector(Connector hitConnector)
	{
		if (hitConnector != _preSlectedConnector)
		{
			EventManager.RaiseEvent<IPreSelectCanceled>(k => k.OnPreAddCancelled(_preSlectedConnector));
			EventManager.RaiseEvent<IPreSelectOnMouseOver>(k => k.OnPreSelect(_selectedConnector, hitConnector));
			_preSlectedConnector = hitConnector;
		}
	}

	private bool CancelPreviewSelectIfNoConnectorHit(Connector hitConnector)
	{
		if (hitConnector == null)
		{
			EventManager.RaiseEvent<IPreSelectCanceled>(k => k.OnPreAddCancelled(_preSlectedConnector));
			_preSlectedConnector = null;
			return true;
		}

		return false;
	}

	private void HandleButtonReleased()
	{
		if (Input.GetMouseButtonUp(0))
		{
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				var hitConnector = hit.transform.GetComponent<Connector>();
				if (hitConnector != _selectedConnector && hitConnector != null) SelectSecondConnector(hitConnector);
			}
			else
			{
				EventManager.RaiseEvent<IDeselectOnClick>(k => k.OnDeselect());
			}

			DestroyPreviewLink();
		}
	}

	private void DestroyPreviewLink()
	{
		if (_previewLink == null) return;
		Destroy(_previewLink.gameObject);
		_previewLink = null;
	}

	private void CreatePreviewLink()
	{
		if (_previewLink != null) return;
		var gameObjectLink = Instantiate(_previewLinkPrefab, transform.localPosition, Quaternion.identity, transform);
		_previewLink = gameObjectLink.GetComponent<PreviewLink>();
		_previewLink.Init(_selectedConnector);
	}
}