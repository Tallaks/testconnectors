public interface IDeselectOnClick : IEventListener
{
	void OnDeselect();
}