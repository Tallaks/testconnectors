public interface ISelectOnClick : IEventListener
{
	void OnClickSelection(Connector selectedConnector);
}