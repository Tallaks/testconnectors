using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ConnectableSpawner : MonoBehaviour
{
	[SerializeField] private GameObject _connectablePrefab;
	[SerializeField] private Transform _connectablesContainer;
	private Main _mainComponent;

	private void Start()
	{
		InitializeMainField();
		SpawnConnectables();
	}

	private void SpawnConnectables()
	{
		for (var index = 0; index < _mainComponent.ConnectableNumber; index++)
		{
			var randomPositionOnCircle = Random.insideUnitCircle * _mainComponent.Radius;
			var spawnXCoord = randomPositionOnCircle.x;
			var spawnZCoord = randomPositionOnCircle.y;

			var spawnPosition = new Vector3(spawnXCoord, 0, spawnZCoord) + transform.position;
			Instantiate(_connectablePrefab, spawnPosition, Quaternion.identity, _connectablesContainer);
		}
	}

	private void InitializeMainField()
	{
		_mainComponent = GetComponentInParent<Main>() == null ? FindObjectOfType<Main>() : GetComponentInParent<Main>();
		if (_mainComponent == null) throw new NullReferenceException("Main component not found");
	}
}