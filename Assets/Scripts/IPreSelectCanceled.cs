public interface IPreSelectCanceled : IEventListener
{
	void OnPreAddCancelled(Connector preAddedConnector);
}