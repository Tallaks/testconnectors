public interface IPreSelectOnMouseOver : IEventListener
{
	void OnPreSelect(Connector from, Connector preSelectedConnector);
}